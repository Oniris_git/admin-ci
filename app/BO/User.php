<?php
/**
 * Created by PhpStorm.
 * User: felicien.arnault
 * Date: 28/10/2021
 * Time: 15:44
 */

namespace app\BO;


class User
{
    use AppObject;

    private $id;
    private $rank;
    private $email;
    private $firstname;
    private $lastname;
    private $password;
    private $isadmin;
    private $birthday;
    private $nationality;
    private $secu13;
    private $secu2;
    private $ine_number;
    private $ecoleorigin;
    private $ecoleoriginp;
    private $phone;
    private $address1;
    private $address2;
    private $address3;
    private $address4;
    private $address5;
    private $centrexamen;
    private $centrexamenp;
    private $c1;
    private $c2;
    private $c3;
    private $c4;
    private $rec1_auth;
    private $rec1_email;
    private $rec2_auth;
    private $rec2_email;
    private $isstaff;
    private $the_guy_folder;
    private $the_guy_photo;
    private $the_guy_file1;
    private $the_guy_file2;
    private $the_guy_file3;
    private $the_guy_file4;
    private $bank_t;
    private $bank_e;
    private $bank_c;





    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }


    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->isadmin !== null;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin)
    {
        $this->isadmin = $admin;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return mixed
     */
    public function getSecu13()
    {
        return $this->secu13;
    }

    /**
     * @param mixed $secu13
     */
    public function setSecu13($secu13)
    {
        $this->secu13 = $secu13;
    }

    /**
     * @return mixed
     */
    public function getSecu2()
    {
        return $this->secu2;
    }

    /**
     * @param mixed $secu2
     */
    public function setSecu2($secu2)
    {
        $this->secu2 = $secu2;
    }

    /**
     * @return mixed
     */
    public function getIneNumber()
    {
        return $this->ine_number;
    }

    /**
     * @param mixed $ine_number
     */
    public function setIneNumber($ine_number)
    {
        $this->ine_number = $ine_number;
    }

    /**
     * @return mixed
     */
    public function getEcoleorigin()
    {
        return $this->ecoleorigin;
    }

    /**
     * @param mixed $ecoleorigin
     */
    public function setEcoleorigin($ecoleorigin)
    {
        $this->ecoleorigin = $ecoleorigin;
    }

    /**
     * @return mixed
     */
    public function getEcoleoriginp()
    {
        return $this->ecoleoriginp;
    }

    /**
     * @param mixed $ecoleoriginp
     */
    public function setEcoleoriginp($ecoleoriginp)
    {
        $this->ecoleoriginp = $ecoleoriginp;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param mixed $address1
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }

    /**
     * @return mixed
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param mixed $address2
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    }

    /**
     * @return mixed
     */
    public function getAddress3()
    {
        return $this->address3;
    }

    /**
     * @param mixed $address3
     */
    public function setAddress3($address3)
    {
        $this->address3 = $address3;
    }

    /**
     * @return mixed
     */
    public function getAddress4()
    {
        return $this->address4;
    }

    /**
     * @param mixed $address4
     */
    public function setAddress4($address4)
    {
        $this->address4 = $address4;
    }

    /**
     * @return mixed
     */
    public function getAddress5()
    {
        return $this->address5;
    }

    /**
     * @param mixed $address5
     */
    public function setAddress5($address5)
    {
        $this->address5 = $address5;
    }

    /**
     * @return mixed
     */
    public function getCentrexamen()
    {
        return $this->centrexamen;
    }

    /**
     * @param mixed $centrexamen
     */
    public function setCentrexamen($centrexamen)
    {
        $this->centrexamen = $centrexamen;
    }

    /**
     * @return mixed
     */
    public function getCentrexamenp()
    {
        return $this->centrexamenp;
    }

    /**
     * @param mixed $centrexamenp
     */
    public function setCentrexamenp($centrexamenp)
    {
        $this->centrexamenp = $centrexamenp;
    }

    /**
     * @return mixed
     */
    public function getC1()
    {
        return $this->c1;
    }

    /**
     * @param mixed $c1
     */
    public function setC1($c1)
    {
        $this->c1 = $c1;
    }

    /**
     * @return mixed
     */
    public function getC2()
    {
        return $this->c2;
    }

    /**
     * @param mixed $c2
     */
    public function setC2($c2)
    {
        $this->c2 = $c2;
    }

    /**
     * @return mixed
     */
    public function getC3()
    {
        return $this->c3;
    }

    /**
     * @param mixed $c3
     */
    public function setC3($c3)
    {
        $this->c3 = $c3;
    }

    /**
     * @return mixed
     */
    public function getC4()
    {
        return $this->c4;
    }

    /**
     * @param mixed $c4
     */
    public function setC4($c4)
    {
        $this->c4 = $c4;
    }

    /**
     * @return mixed
     */
    public function getRec1Auth()
    {
        return $this->rec1_auth;
    }

    /**
     * @param mixed $rec1_auth
     */
    public function setRec1Auth($rec1_auth)
    {
        $this->rec1_auth = $rec1_auth;
    }

    /**
     * @return mixed
     */
    public function getRec1Email()
    {
        return $this->rec1_email;
    }

    /**
     * @param mixed $rec1_email
     */
    public function setRec1Email($rec1_email)
    {
        $this->rec1_email = $rec1_email;
    }

    /**
     * @return mixed
     */
    public function getRec2Auth()
    {
        return $this->rec2_auth;
    }

    /**
     * @param mixed $rec2_auth
     */
    public function setRec2Auth($rec2_auth)
    {
        $this->rec2_auth = $rec2_auth;
    }

    /**
     * @return mixed
     */
    public function getRec2Email()
    {
        return $this->rec2_email;
    }

    /**
     * @param mixed $rec2_email
     */
    public function setRec2Email($rec2_email)
    {
        $this->rec2_email = $rec2_email;
    }

    /**
     * @return mixed
     */
    public function isstaff()
    {
        return $this->isstaff;
    }

    /**
     * @param mixed $isstaff
     */
    public function setStaff($isstaff)
    {
        $this->isstaff = $isstaff;
    }

    /**
     * @return mixed
     */
    public function getTheGuyFolder()
    {
        return $this->the_guy_folder;
    }

    /**
     * @param mixed $the_guy_folder
     */
    public function setTheGuyFolder($the_guy_folder)
    {
        $this->the_guy_folder = $the_guy_folder;
    }

    /**
     * @return mixed
     */
    public function getTheGuyPhoto()
    {
        return $this->the_guy_photo;
    }

    /**
     * @param mixed $the_guy_photo
     */
    public function setTheGuyPhoto($the_guy_photo)
    {
        $this->the_guy_photo = $the_guy_photo;
    }

    /**
     * @return mixed
     */
    public function getTheGuyFile1()
    {
        return $this->the_guy_file1;
    }

    /**
     * @param mixed $the_guy_file1
     */
    public function setTheGuyFile1($the_guy_file1)
    {
        $this->the_guy_file1 = $the_guy_file1;
    }

    /**
     * @return mixed
     */
    public function getTheGuyFile2()
    {
        return $this->the_guy_file2;
    }

    /**
     * @param mixed $the_guy_file2
     */
    public function setTheGuyFile2($the_guy_file2)
    {
        $this->the_guy_file2 = $the_guy_file2;
    }

    /**
     * @return mixed
     */
    public function getTheGuyFile3()
    {
        return $this->the_guy_file3;
    }

    /**
     * @param mixed $the_guy_file3
     */
    public function setTheGuyFile3($the_guy_file3)
    {
        $this->the_guy_file3 = $the_guy_file3;
    }

    /**
     * @return mixed
     */
    public function getTheGuyFile4()
    {
        return $this->the_guy_file4;
    }

    /**
     * @param mixed $the_guy_file4
     */
    public function setTheGuyFile4($the_guy_file4)
    {
        $this->the_guy_file4 = $the_guy_file4;
    }



    public function toString() {
        return $this->getFirstname().'&nbsp;'.$this->getLastname();
    }



}