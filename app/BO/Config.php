<?php
/**
 * Created by PhpStorm.
 * User: felicien.arnault
 * Date: 23/11/2021
 * Time: 11:17
 */

namespace app\BO;


class Config
{
    use AppObject;

    private $id = 1;
    private $stamp_reg_start;
    private $stamp_reg_ends;
    private $stamp_reg_sitecloses;
    private $stamp_exam;
    private $stamp_run_array;
    private $max_A;
    private $max_L;
    private $max_N;
    private $max_T;
    private $A_chief;
    private $L_chief;
    private $N_chief;
    private $T_chief;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStampRegStart()
    {
        return $this->stamp_reg_start;
    }

    /**
     * @param mixed $stamp_reg_start
     */
    public function setStampRegStart($stamp_reg_start)
    {
        $this->stamp_reg_start = $stamp_reg_start;
    }

    /**
     * @return mixed
     */
    public function getStampRegEnds()
    {
        return $this->stamp_reg_ends;
    }

    /**
     * @param mixed $stamp_reg_ends
     */
    public function setStampRegEnds($stamp_reg_ends)
    {
        $this->stamp_reg_ends = $stamp_reg_ends;
    }

    /**
     * @return mixed
     */
    public function getStampRegSitecloses()
    {
        return $this->stamp_reg_sitecloses;
    }

    /**
     * @param mixed $stamp_reg_sitecloses
     */
    public function setStampRegSitecloses($stamp_reg_sitecloses)
    {
        $this->stamp_reg_sitecloses = $stamp_reg_sitecloses;
    }

    /**
     * @return mixed
     */
    public function getStampExam()
    {
        return $this->stamp_exam;
    }

    /**
     * @param mixed $stamp_exam
     */
    public function setStampExam($stamp_exam)
    {
        $this->stamp_exam = $stamp_exam;
    }

    /**
     * @return mixed
     */
    public function getStampRunArray()
    {
        return $this->stamp_run_array;
    }

    /**
     * @param mixed $stamp_run_array
     */
    public function setStampRunArray($stamp_run_array)
    {
        $this->stamp_run_array = $stamp_run_array;
    }

    /**
     * @return mixed
     */
    public function getMaxA()
    {
        return $this->max_A;
    }

    /**
     * @param mixed $max_A
     */
    public function setMaxA($max_A)
    {
        $this->max_A = $max_A;
    }

    /**
     * @return mixed
     */
    public function getMaxL()
    {
        return $this->max_L;
    }

    /**
     * @param mixed $max_L
     */
    public function setMaxL($max_L)
    {
        $this->max_L = $max_L;
    }

    /**
     * @return mixed
     */
    public function getMaxN()
    {
        return $this->max_N;
    }

    /**
     * @param mixed $max_N
     */
    public function setMaxN($max_N)
    {
        $this->max_N = $max_N;
    }

    /**
     * @return mixed
     */
    public function getMaxT()
    {
        return $this->max_T;
    }

    /**
     * @param mixed $max_T
     */
    public function setMaxT($max_T)
    {
        $this->max_T = $max_T;
    }

    /**
     * @return mixed
     */
    public function getAChief()
    {
        return $this->A_chief;
    }

    /**
     * @param mixed $A_chief
     */
    public function setAChief($A_chief)
    {
        $this->A_chief = $A_chief;
    }

    /**
     * @return mixed
     */
    public function getLChief()
    {
        return $this->L_chief;
    }

    /**
     * @param mixed $L_chief
     */
    public function setLChief($L_chief)
    {
        $this->L_chief = $L_chief;
    }

    /**
     * @return mixed
     */
    public function getNChief()
    {
        return $this->N_chief;
    }

    /**
     * @param mixed $N_chief
     */
    public function setNChief($N_chief)
    {
        $this->N_chief = $N_chief;
    }

    /**
     * @return mixed
     */
    public function getTChief()
    {
        return $this->T_chief;
    }

    /**
     * @param mixed $T_chief
     */
    public function setTChief($T_chief)
    {
        $this->T_chief = $T_chief;
    }



}