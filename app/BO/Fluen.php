<?php
/**
 * Created by PhpStorm.
 * User: felicien.arnault
 * Date: 23/11/2021
 * Time: 11:20
 */

namespace app\BO;


class Fluen
{
    use AppObject;

    private $id;
    private $run_id;
    private $user;
    private $submission;
    private $answer;
    private $validate;
    private $valip;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRunId()
    {
        return $this->run_id;
    }

    /**
     * @param mixed $run_id
     */
    public function setRunId($run_id)
    {
        $this->run_id = $run_id;
    }


    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getSubmission()
    {
        return $this->submission;
    }

    /**
     * @param mixed $submission
     */
    public function setSubmission($submission)
    {
        $this->submission = $submission;
    }

    /**
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param mixed $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return mixed
     */
    public function getValidate()
    {
        return $this->validate;
    }

    /**
     * @param mixed $validate
     */
    public function setValidate($validate)
    {
        $this->validate = $validate;
    }

    /**
     * @return mixed
     */
    public function getValip()
    {
        return $this->valip;
    }

    /**
     * @param mixed $valip
     */
    public function setValip($valip)
    {
        $this->valip = $valip;
    }


}