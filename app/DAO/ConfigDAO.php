<?php
/**
 * Created by PhpStorm.
 * User: felicien.arnault
 * Date: 23/11/2021
 * Time: 11:35
 */

namespace app\DAO;


use app\BO\Config;

class ConfigDAO extends DAO
{
    protected $table = 'ci_config';
    protected $prefix = '';

    public function find($params, $force_array = false, $joins = false)
    {
        $datas = parent::find($params, true, false);
        $return = [];

        foreach ($datas as $row) {
            $user = new Config($row);

            $return[] = $user;
        }

        return $this->force_array($return, $force_array);
    }
}