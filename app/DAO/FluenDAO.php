<?php
/**
 * Created by PhpStorm.
 * User: felicien.arnault
 * Date: 23/11/2021
 * Time: 11:24
 */

namespace app\DAO;


use app\BO\Fluen;

class FluenDAO extends DAO
{

    protected $table = 'ci_fluens';
    protected $prefix = '';

    public function find($params, $force_array = false, $joins = false)
    {
        $joins = !$joins ? [] : $joins;

        $joins = array_merge($joins, [
            'ci_users' => [
                'join' => 'LEFT',
                'id' => 'user_id',
            ]
        ]);
        $datas = parent::find($params, true, $joins);
        $return = [];

        foreach ($datas as $row) {
            $fluen = new Fluen($row);
            $user = new User($row);

            $fluen->setUser($user);

            $return[] = $fluen;
        }


        return $this->force_array($return, $force_array);
    }

}