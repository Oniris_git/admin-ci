<?php
/**
 * Created by PhpStorm.
 * User: felicien.arnault
 * Date: 27/10/2021
 * Time: 10:01
 */

namespace app\DAO;

use app\BO\User;
use Vespula\Auth\Adapter\AdapterInterface;
use Vespula\Auth\Exception;


class UserDAO extends DAO implements AdapterInterface
{
    protected $table = 'ci_users';
    protected $prefix = '';

    public function find($params, $force_array = false, $joins = false)
    {
        $joins = !$joins ? [] : $joins;

        $datas = parent::find($params, true, $joins);
        $return = [];

        foreach ($datas as $row) {
            $user = new User($row);

            $return[] = $user;
        }


        return $this->force_array($return, $force_array);
    }

    /**
     * validate the username and password.
     *
     * @param array $credentials Array with keys 'username' and 'password'
     * @return boolean
     * @throws Exception
     */
    public function authenticate(array $credentials)
    {
        $request = "SELECT * FROM ".$this->table." WHERE email = :username";

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':username' => $credentials['username']
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row) {
        }
        if (!empty($row)) {
            $check = $credentials['password'] === $row['password'];
            if ($check == false) {
                throw new Exception("Mauvaise combinaison Identifiant/Mot de passe");
            }
            return $check;
        } else {
            throw new Exception("Mauvaise combinaison Identifiant/Mot de passe");
        }
    }

    /**
     * Find extra userdata. This will be stored in the session
     *
     * @param $username
     * @return User Userdata specific to the adapter
     */
    public function lookupUserData($username)
    {
        $request = "SELECT * FROM ".$this->table." WHERE email = :username;";

        $stmt = $this->getPDO(true)->prepare($request);
        $stmt->execute([
            ':username' => $username
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            $user = new User($row);
            return $user;
        }
    }

    public function getError()
    {
        // TODO: Implement getError() method.
    }

}