<?php
/**
 * Created by PhpStorm.
 * User: felicien.arnault
 * Date: 23/11/2021
 * Time: 11:32
 */

$renderer->header('Accueil')
    ->open_body([
        [
            'tag' => 'div',
            'class' => 'content-center'
        ]
    ], $USER, true)
    ->links()
    ->close_body($USER)
    ->footer()
    ->render();