<?php
/**
 * Created by PhpStorm.
 * User: felicien.arnault
 * Date: 23/11/2021
 * Time: 12:02
 */


$renderer->header('Accueil')
    ->open_body([
        [
            'tag' => 'div',
            'class' => 'content-center'
        ]
    ], $USER, true)
    ->close_body($USER)
    ->footer()
    ->render();