<?php


use app\DAO\UserDAO;

require __DIR__.'/../app/Init.php';
require __DIR__.'/controllers/lib.php';
$args = [
    "page" => FILTER_SANITIZE_STRING
];
$GET = filter_input_array(INPUT_GET, $args, false);

$user_dao = new UserDAO($db_connector);

// if (!isset($from)) {
//     $from = 'home';
// }
$USER = null;
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//rooting
if(isset($GET['page'])) {
    $page = $GET['page'];
    if (strpos($page, '_')) {
        $parts = explode('_', $page);
        $page = '';
        foreach ($parts as $part) {
            $page .= ucfirst($part);
        }
    }
    if (!page_exist($page)) {
        error_redirect('404', $from);
    }
    // If is not connected
    if ($auth->isAnon() || $SESSION->getUserdata() === null) {
        // Page that can be visited without authentication
        $can_access = ['login', 'success', 'error', 'reset', 'logout', 'signin'];
        // Redirect to login
        if (!in_array($page, $can_access)) {
            $from = $page;
            $page = 'login';
        }
    } else {
        $USER = $SESSION->getUserData();
        // Check rights
        if (!can_access($GET['page'], $USER)) {
            error_redirect('401', $from, $USER);
        }
    }
} else {
    $page = $auth->isAnon() || $SESSION->getUserdata() === null ? 'login' : 'home';
}

if ($page === 'home') {
    if ($auth->isAnon()) {
        $page = 'login';
    } else {
        $page = $USER->isAdmin() ? 'home' : 'error';
    }
}

// Load renderer and controller
$controller = ucfirst($page).'Controller.php';
$page = ucfirst($page);
$to_render = $page;
$to_load = $controller;

$renderer = renderers\Provider::get_renderer($to_render);
require __DIR__.'/controllers/'.$to_load;