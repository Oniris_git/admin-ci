<?php

namespace renderers;

class LoginRenderer extends BaseRenderer
{
    /**
     * @param string $token login token
     * @return self
     */
    public function login_form($token, $from = 'home') {
        $this->output .=' <div class="d-flex justify-content-center">
        <div id="formLogin"  class="w-100 d-flex justify-content-center">
            <form action="?page=login" method="post" id="form_login">
            <img src="'.$GLOBALS['domain'].'/public/style/resources/oniris.jpg"><br><br>
            <h1>Administration des coucours d\'internat</h1><br>
                <div class="mx-sm-3 mb-2">
                    <div class="mx-sm-3 mb-2">
                        <input type="text" placeholder="Adresse mail" class="form-control width100" name="username" autofocus>
                    </div>
                </div>
                <div class="mx-sm-3 mb-2">
                    <div class="mx-sm-3 mb-2">
                        <input type="password" placeholder="Mot de passe" class="form-control width100" name="password">
                    </div>
                </div>
                <input type="hidden" name="from" value="'.$from.'">
                <br>
                <div class="row justify-content-center">
                    <div class="">
                        <input type="hidden" name="token" value="'.$token.'">
                        <button type="submit" class="btn btn-outline-success width100">Connexion
                                </button>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                            </form>
                            </div>
                        </div>
                     </div>
                 </div>
             </div>';
             return $this;
    }
}