<?php
/**
 * Created by PhpStorm.
 * User: felicien.arnault
 * Date: 23/11/2021
 * Time: 11:33
 */

namespace renderers;


class HomeRenderer extends BaseRenderer
{
    public function links() {
        $this->output .= '<div class="col-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 3%">
	            <div class="row justify-content-start">
                            </div>
	                        <div class="row justify-content_between" style="padding: 1rem 1rem 0 1rem; margin-bottom:1rem; margin-top:2rem">
                                <div class="admin_icon" id="shootMarges">
                                    <a href="?page=registrations" class="btn btn-default">
                                        <i class="fas fa-folder-open iconMenu btnHover"></i>
                                    </a>
                                    <div class="col-12 center" style="margin-top:1rem;margin-bottom:2rem">
                                        Dossiers
                                    </div>
                                </div>  
                                <div class="admin_icon" id="shootMarges">
                                    <a href="index.php?page=choices" class="btn btn-default">
                                        <i class="fas fa-list iconMenu btnHover"></i>
                                    </a>
                                    <div class="col-12 center" style="margin-top:1rem;margin-bottom:2rem">
                                        Choix
                                    </div>
                                </div>                             
                                <div class="admin_icon" id="shootMarges">
                                    <a href="index.php?page=settings" class="btn btn-default">
                                        <i class="fas fa-cogs iconMenu btnHover"></i>
                                    </a>
                                    <div class="col-12 center" style="margin-top:1rem;margin-bottom:2rem">
                                        Paramètres
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div>';
        return $this;
    }
}