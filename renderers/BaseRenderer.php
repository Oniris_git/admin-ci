<?php

namespace renderers;
use App\Session;
use League\Container\Container;
use renderers\lib\Format;

abstract class BaseRenderer
{
    protected $opened_tags;
    protected $output;
    protected const STYLE_DIRECTORY = __DIR__.'/../public/style/';
    protected $from;

    public function __construct()
    {
        $this->output = '';
        $this->opened_tags = [];
    }

    //--- HTML ---

    /**
     * Html header
     * @param string $title - Html title
     * @return self
     */
    public function header($title = 'Accueil'){
        $this->output .= '<!DOCTYPE html>
                            <html lang="fr" id="html">
                                <head>
                                <link rel="stylesheet" href="../vendor/components/font-awesome/css/all.css">';
        $this->style('css/lib')
                ->style('css/app');
        $this->output .= '<title>Administration concours internat - '.$title.'</title>
            </head>';

        return $this;
    }
    /**
     * Html footer
     * @return self
     */
    public function footer(){
        $this->output .= '<footer>';
        $this->style('js/lib')
            ->style('js/app');
        $this->output .= '</footer>';

        return $this;
    }


    /**
     * display error
     * @param string $e Error message or true if no error
     * @return self
     */
    public function error($passed) {
        if ($passed !== true) {
            $this->output .= '<div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <p style="text-align: center"><strong>'.$passed.'</strong></p>
            </div>';
        }
        return $this;
    }

    public function valid($valid){
        if ($valid !== false ) {
        $this->output .= '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <p style="text-align: center"><strong>'.$valid.'</strong></p>
                </div>';
        }
        return $this;
    }

    /**
     * @param string $subdirectory Style subdirectory name; base folder : public/style
     * @return self
     */
    private function style(string $subdirectory){
        $domain = str_replace($_SERVER['DOCUMENT_ROOT'],'',__DIR__);
        $domain = str_replace('/renderers', '', $domain);
        $domain = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$domain;
        $styledirectory = self::STYLE_DIRECTORY.$subdirectory;
        $files = array_diff(scandir($styledirectory), array('..', '.'));
        foreach ($files as $stylefile) {
            if ($this->endsWith($stylefile, '.css')) {
                $this->output .= '<link rel="stylesheet" href="'.$domain.'/public/style/'.$subdirectory.'/'.$stylefile.'">';
            } else {
                $this->output .= '<script src="'.$domain.'/public/style/'.$subdirectory.'/'.$stylefile.'"></script>';
            }
        }
        return $this;
    }
    /**
     * Redirect button to $from
     * @param string $from
     * @return self
     */
    public function previous_page($from) {
        $this->output .= '<div class="d-flex flex-row w-100">
                                <div class="homeIcon justify-content-start">
                                    <a href="index.php?page='.$from.'">
                                        <i class="fas fa-arrow-left leftArrow" style="margin:0;"></i>
                                    </a>
                                </div>
                            </div>';
        return $this;
    }

    public function notify($notify) {
        if($notify !== false) {
            $this->output .= '<script>show_toast(\'success\', \''.$notify.'\')</script>';
        }
        return $this;
    }

    public function title($title, $type = 1) {
        $this->output .= '<h'.$type.' class="text-center">'.$title.'</h'.$type.'><br><br>';
        return $this;
    }

    public function app_modal($title, $id) {
        $this->output .= '<div class="modal fade" aria-labelledby="manualModalLabel" id="'.$id.'" style="margin-bottom: 1rem"  tabindex="-1" role="dialog" aria-hidden="true">
            <div  class="modal-dialog modal-lg" role="document" id="formManual">
                <div class="modal-content" style="margin-top: 33%">
                    <div class="modal-header">
                        <h5 class="modal-title" id="manualModalLabel"><span id="modal-name">'.$title.'</span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modal-content">

                    
                    </div>
                </div>
            </div>
        </div>';
        return $this;
    }


    /**
     * Open body with optional params
     * @param array $tags : Optionnal tags - array([tag] => [value], [attribute] => [value], ...)
     * @param bool $USER : User role, false if no navbar, default manager
     * @return self
     */
    public function open_body(array $tags = [], $USER, $home = false) {
        $this->output .='<body>';
        if ($USER !== false) {
            if ($home === true) {
                $this->output .= '<div class="home-container">';
                $this->opened_tags[] = 'div';
            } else {
                $this->output .= '<div class="app-container">';
                $this->opened_tags[] = 'div';
            }
        } else {
            $this->output .= '<div class="login-container">';
            $this->opened_tags[] = 'div';
        }
        foreach ($tags as $attributes) {
            $this->output .= '<'.$attributes['tag'];
            foreach ($attributes as $key => $value) {
                if ($key !== 'tag') {
                    $this->output .= ' '.$key.'="'.$value.'"';
                }
            }
            $this->output .= '>';
            $this->opened_tags[] = $attributes['tag'];
        }

        return $this;
    }

    /**
     * Close body and optionnals tags opened in open_body()
     * @return self
     */
    public function close_body($USER = null) {
        if ($USER !== null) {
            $this->output .= '<a href="?page=logout" class="signout-btn" title="Déconnexion">
                                <i class="fas fa-2x fa-sign-out-alt"></i>
                            </a>';
        }
        foreach (array_reverse($this->opened_tags) as $tag) {
            $this->output .= '</'.$tag.'>';
        }
        $this->output .= '</body>';
        return $this;
    }

    public function set_referer($from) {
        $this->from = $from;

        return $this;
    }

    public function get_referer() {
        return $this->from;
    }

    //--- ---

    /**
     * Format and display HTML contained in $this->output
     */
    public function render() {
        $format = new Format;
        echo $format->HTML($this->output);
    }

    /**
     * TODO : delete and use str_ends_with() when update to PHP 8
     */
    private function endsWith(string $haystack,string $needle ) {
        $length = strlen( $needle );
        if( !$length ) {
            return true;
        }
        return substr( $haystack, -$length ) === $needle;
    }

    protected function active($page) {
        return $this->from === $page ? 'active' : '';
    }
}